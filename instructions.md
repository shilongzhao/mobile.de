1. run `docker compose up -d db` to start DB
2. run `./gradlew flywayMigrate` to migrate DB
3. run `./gradlew generateJooq` to generate jooq code
4. run `./gradlew test` to run all tests
5. run `./gradlew bootRun` to start the app