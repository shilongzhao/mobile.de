package de.mobile

import com.fasterxml.jackson.databind.ObjectMapper
import de.mobile.service.AdService
import org.jooq.DSLContext
import org.jooq.generated.tables.references.MOBILE_AD
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@SpringBootTest
@AutoConfigureMockMvc
class AdResourceTest @Autowired constructor(
    val dslContext: DSLContext,
    val adService: AdService,
    val mvc: MockMvc
) {

    @BeforeEach
    fun init() {
        dslContext.deleteFrom(MOBILE_AD).execute()
    }

    @AfterEach
    fun clean() {
        init()
    }

    @Test
    fun `create an ad should return its id and get by id should return the ticket`() {
        val jsonString = """
            {
            "id": 0,
            "make": "Mazda", 
            "model":"MX-5", 
            "description":"roadster", 
            "category":"CAR", 
            "price":"29999.99"
            }
            """.trimIndent()
        val createResp = mvc.perform(MockMvcRequestBuilders.post("/ads")
            .content(jsonString).contentType(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()
        val id = createResp.response.contentAsString.toLong()

        val getResp = mvc.perform(MockMvcRequestBuilders.get("/ads/$id"))
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()

        val jsonResp = ObjectMapper().readTree(getResp.response.contentAsString)
        assertEquals("Mazda", jsonResp["make"].asText())
        assertEquals("29999.99", jsonResp["price"].asText())

    }
}