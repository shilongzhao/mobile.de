package de.mobile

import de.mobile.repo.JooqAdRepository
import org.jooq.DSLContext
import org.jooq.generated.tables.references.MOBILE_AD
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class JooqAdRepositoryTest @Autowired constructor(
    val dslContext: DSLContext,
    val jooqAdRepository: JooqAdRepository) {

    @BeforeEach
    fun init() {
        dslContext.deleteFrom(MOBILE_AD).execute()
    }

    @AfterEach
    fun clear() {
        init()
    }

    @Test
    fun `get an ad by id should be identical to the created`() {
        val ad = MobileAdTestFactory.createAd(model = "500X")
        val id = jooqAdRepository.create(ad)
        val created = jooqAdRepository.get(id)
        assertEquals(id, created.id)
        assertEquals(ad.model, created.model)
        assertEquals(ad.make, created.make)
        assertEquals(ad.category, created.category)
        assertEquals(ad.description, created.description)
        assertEquals(ad.price, created.price)
    }

    @Test
    fun `list should return all created ads`() {
        val ad1 = MobileAdTestFactory.createAd(make = "Citroen")
        val ad2 = MobileAdTestFactory.createAd(make = "Peugeot")

        val id1 = jooqAdRepository.create(ad1)
        val id2 = jooqAdRepository.create(ad2)

        val created1 = jooqAdRepository.get(id1)
        val created2 = jooqAdRepository.get(id2)
        val list = jooqAdRepository.list()
        assertTrue(list.containsAll(listOf(created1, created2)))
    }
}