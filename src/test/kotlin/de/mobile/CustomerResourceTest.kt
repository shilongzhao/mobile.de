package de.mobile

import com.fasterxml.jackson.databind.ObjectMapper
import org.jooq.DSLContext
import org.jooq.generated.tables.references.MOBILE_AD
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@SpringBootTest
@AutoConfigureMockMvc
class CustomerResourceTest @Autowired constructor(
    val dslContext: DSLContext,
    val mockMvc: MockMvc
) {
    @BeforeEach
    fun init() {
        dslContext.deleteFrom(MOBILE_AD).execute()
    }

    @AfterEach
    fun clean() {
        init()
    }

    @Test
    fun `create a customer should be successful`() {
        val json = """
            {"id":0,"firstName":"erlang","lastName":"shell","companyName":"coda.io","email":"hello@coda.io"}
            """.trimIndent()
        val createResp = mockMvc.perform(
            MockMvcRequestBuilders
                .post("/customers")
                .content(json)
                .contentType(MediaType.APPLICATION_JSON)
        )
            .andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()
        val id = createResp.response.contentAsString.toLong()

        val getResp = mockMvc.perform(
            MockMvcRequestBuilders.get("/customers/$id")
        ).andExpect(MockMvcResultMatchers.status().isOk)
            .andReturn()
        val readJson = ObjectMapper().readTree(getResp.response.contentAsString)

        Assertions.assertEquals("erlang", readJson["firstName"].asText())
        Assertions.assertEquals("shell", readJson["lastName"].asText())
        Assertions.assertEquals("coda.io", readJson["companyName"].asText())
    }
}