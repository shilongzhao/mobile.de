package de.mobile

import de.mobile.entity.Category
import de.mobile.entity.MobileAd
import java.math.BigDecimal

object MobileAdTestFactory {
    fun createAd(make: String = "FIAT",
                 model:String = "500",
                 description: String = "",
                 category: Category = Category.CAR,
                 price: BigDecimal = BigDecimal("25000")) =
        MobileAd(0, make, model, description, category, price)
}