package de.mobile.config

import java.util.Locale

interface LocaleProvider {
    fun get(): Locale
}