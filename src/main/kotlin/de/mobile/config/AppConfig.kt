package de.mobile.config

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.i18n.LocaleContextHolder
import java.util.*

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = ["de.mobile"])
class AppConfig {
    @Bean
    fun localeProvider(): LocaleProvider {
        return object : LocaleProvider {
            override fun get(): Locale {
                return LocaleContextHolder.getLocale()
            }
        }
    }
}