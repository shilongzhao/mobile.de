package de.mobile.rest

import de.mobile.dto.MobileAdDto
import de.mobile.dto.toEntity
import de.mobile.entity.toDto
import de.mobile.service.AdService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/ads")
class AdResource(val adService: AdService) {
    @GetMapping("/{id}")
    fun get(@PathVariable("id") id: Long): MobileAdDto {
        return adService.get(id).toDto()
    }

    @GetMapping
    fun list(): List<MobileAdDto> {
        return adService.list().map { it.toDto() }
    }

    @PostMapping
    fun create(@RequestBody mobileAdDto: MobileAdDto): Long {
        return adService.create(mobileAdDto.toEntity())
    }
}