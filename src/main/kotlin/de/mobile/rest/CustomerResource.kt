package de.mobile.rest

import de.mobile.dto.MobileCustomerDto
import de.mobile.dto.toEntity
import de.mobile.entity.MobileCustomer
import de.mobile.service.CustomerService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/customers")
class CustomerResource(val customerService: CustomerService) {
    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): MobileCustomer {
        return customerService.get(id)
    }

    @PostMapping
    fun create(@RequestBody mobileCustomer: MobileCustomerDto): Long {
        return customerService.create(mobileCustomer.toEntity())
    }
}
