package de.mobile.rest

import de.mobile.repo.error.EntityNotFoundException
import de.mobile.repo.error.InternalDatabaseException
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@RestControllerAdvice
class MobileControllerAdvice : ResponseEntityExceptionHandler() {

    @ExceptionHandler(value = [EntityNotFoundException::class])
    fun notFound(e: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        val errorResponse = ErrorResponse(HttpStatus.NOT_FOUND.value(), e.message ?: "Error not specified")
        return handleExceptionInternal(e, errorResponse, HttpHeaders(), HttpStatus.NOT_FOUND, request)
    }

    @ExceptionHandler(value = [InternalDatabaseException::class])
    fun internalError(e: RuntimeException, request: WebRequest): ResponseEntity<Any> {
        val errorResponse = ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.message ?: "Error not specified")
        return handleExceptionInternal(e, errorResponse, HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request)
    }
}

class ErrorResponse(val status: Int, val message: String)