package de.mobile.service

import de.mobile.entity.MobileAd
import de.mobile.repo.JooqAdRepository
import org.springframework.stereotype.Service

@Service
class DatabaseAdService(val jooqAdRepository: JooqAdRepository): AdService {
    override fun create(ad: MobileAd): Long {
        return jooqAdRepository.create(ad)
    }

    override fun get(id: Long): MobileAd {
        return jooqAdRepository.get(id)
    }

    override fun list(): List<MobileAd> {
        return jooqAdRepository.list()
    }
}