package de.mobile.service

import de.mobile.entity.MobileAd


interface AdService {
    fun create(ad: MobileAd): Long
    fun get(id: Long): MobileAd
    fun list(): List<MobileAd>
}