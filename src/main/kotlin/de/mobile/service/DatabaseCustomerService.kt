package de.mobile.service

import de.mobile.entity.MobileCustomer
import de.mobile.repo.CustomerRepository
import org.springframework.stereotype.Service

@Service
class DatabaseCustomerService(val jooqCustomerRepository: CustomerRepository): CustomerService {
    override fun get(id: Long): MobileCustomer {
        return jooqCustomerRepository.get(id)
    }

    override fun create(customer: MobileCustomer): Long {
        return jooqCustomerRepository.create(customer)
    }

    override fun delete(id: Long): Long {
        return jooqCustomerRepository.delete(id)
    }
}