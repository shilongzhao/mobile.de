package de.mobile.service

import de.mobile.entity.MobileCustomer

interface CustomerService {
    fun get(id: Long): MobileCustomer
    fun create(customer: MobileCustomer): Long
    fun delete(id: Long): Long
}