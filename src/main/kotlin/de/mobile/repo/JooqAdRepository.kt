package de.mobile.repo

import de.mobile.entity.Category
import de.mobile.entity.MobileAd
import de.mobile.entity.toRecord
import de.mobile.repo.error.InternalDatabaseException
import de.mobile.repo.error.EntityNotFoundException
import org.jooq.DSLContext
import org.jooq.generated.tables.records.MobileAdRecord
import org.jooq.generated.tables.references.MOBILE_AD
import org.springframework.stereotype.Repository

@Repository
class JooqAdRepository(val dslContext: DSLContext) : AdRepository {
    override fun create(ad: MobileAd): Long {
        val record = ad.toRecord()
        return dslContext.insertInto(MOBILE_AD,
            MOBILE_AD.MAKE, MOBILE_AD.MODEL, MOBILE_AD.DESCRIPTION, MOBILE_AD.CATEGORY, MOBILE_AD.PRICE)
            .values(record.make, record.model, record.description, record.category, record.price)
            .returning(MOBILE_AD.ID)
            .fetchOne()?.id
            ?: throw InternalDatabaseException("failed to insert ad $ad")
    }

    override fun get(adId: Long): MobileAd {
        return dslContext.selectFrom(MOBILE_AD).where(MOBILE_AD.ID.eq(adId))
            .fetchInto(MobileAd::class.java).firstOrNull()
            ?: throw EntityNotFoundException("mobile ad not found $adId")
    }

    override fun list(): List<MobileAd> {
        return dslContext.selectFrom(MOBILE_AD)
            .fetchInto(MobileAd::class.java)
    }

    private fun MobileAdRecord.toEntity() =
        MobileAd(id!!, make!!, model!!, description!!, Category.valueOf(category!!.name), price!!)
}