package de.mobile.repo

import de.mobile.entity.MobileCustomer

interface CustomerRepository {
    fun create(customer: MobileCustomer): Long
    fun get(id: Long): MobileCustomer
    fun delete(id: Long): Long
}