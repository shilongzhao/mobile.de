package de.mobile.repo

import de.mobile.entity.MobileAd

interface AdRepository {
    fun create(ad: MobileAd): Long
    fun get(adId: Long): MobileAd
    fun list(): List<MobileAd>
}