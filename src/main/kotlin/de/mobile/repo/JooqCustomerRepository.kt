package de.mobile.repo

import de.mobile.entity.MobileCustomer
import de.mobile.repo.error.InternalDatabaseException
import de.mobile.repo.error.EntityNotFoundException
import org.jooq.DSLContext
import org.jooq.generated.tables.references.MOBILE_CUSTOMER
import org.springframework.stereotype.Repository

@Repository
class JooqCustomerRepository(val dslContext: DSLContext) : CustomerRepository {
    override fun create(customer: MobileCustomer): Long {
        return dslContext.insertInto(
            MOBILE_CUSTOMER,
            MOBILE_CUSTOMER.FIRST_NAME, MOBILE_CUSTOMER.LAST_NAME,
            MOBILE_CUSTOMER.COMPANY_NAME, MOBILE_CUSTOMER.EMAIL
        )
            .values(
                customer.firstName, customer.lastName,
                customer.companyName, customer.email
            )
            .returning(MOBILE_CUSTOMER.ID)
            .fetchOne()?.id
            ?: throw InternalDatabaseException("failed to create customer $customer")
    }

    override fun get(id: Long): MobileCustomer {
        return dslContext.selectFrom(MOBILE_CUSTOMER)
            .where(MOBILE_CUSTOMER.ID.eq(id))
            .fetchInto(MobileCustomer::class.java).firstOrNull()
            ?: throw EntityNotFoundException("customer $id does not exist")
    }

    override fun delete(id: Long): Long {
        return dslContext.deleteFrom(MOBILE_CUSTOMER)
            .where(MOBILE_CUSTOMER.ID.eq(id))
            .returning(MOBILE_CUSTOMER.ID)
            .fetchOne()?.id
            ?: throw EntityNotFoundException("customer $id does not exist")
    }
}