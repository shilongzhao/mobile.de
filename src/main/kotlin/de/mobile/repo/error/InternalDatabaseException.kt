package de.mobile.repo.error

class InternalDatabaseException(override val message: String?) : RuntimeException(message)
