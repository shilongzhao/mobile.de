package de.mobile.repo.error

class EntityNotFoundException(override val message: String?) : RuntimeException(message)
