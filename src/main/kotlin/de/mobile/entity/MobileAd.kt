package de.mobile.entity

import de.mobile.dto.MobileAdDto
import org.jooq.generated.enums.MobileAdCategory
import org.jooq.generated.tables.records.MobileAdRecord
import java.math.BigDecimal

data class MobileAd(
    val id: Long,
    val make: String,
    val model: String,
    val description: String,
    val category: Category,
    val price: BigDecimal
)

fun MobileAd.toDto() = MobileAdDto(id, make, model, description, category, price)

fun MobileAd.toRecord() = MobileAdRecord(id, make, model, description, MobileAdCategory.valueOf(category.name), price)