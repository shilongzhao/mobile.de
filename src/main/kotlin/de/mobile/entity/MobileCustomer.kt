package de.mobile.entity

data class MobileCustomer(
    val id: Long,
    val firstName: String,
    val lastName: String,
    val companyName: String,
    val email: String,
)