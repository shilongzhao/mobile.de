package de.mobile.entity

enum class Category {
    CAR,
    MOTORBIKE,
    TRUCK
}