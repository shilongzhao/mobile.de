package de.mobile.dto

import de.mobile.entity.MobileCustomer

data class MobileCustomerDto (
    val id: Long,
    val firstName: String,
    val lastName: String,
    val companyName: String,
    val email: String,
)

fun MobileCustomerDto.toEntity() = MobileCustomer(id, firstName, lastName, companyName, email)