package de.mobile.dto

import de.mobile.entity.Category
import de.mobile.entity.MobileAd
import java.math.BigDecimal

data class MobileAdDto(
    val id: Long,
    val make: String,
    val model: String,
    val description: String,
    val category: Category,
    val price: BigDecimal
)

fun MobileAdDto.toEntity() = MobileAd(id, make, model, description, category, price)
